package controllers;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Funcionario;

public class FuncionarioController {
    private Connection connection;
    private Controller controller;
    private PreparedStatement stmt;

    public FuncionarioController() {
        this.connection = new MyConnection().getConnection();
        this.controller = new Controller();
    }
    public ArrayList<Funcionario> fetch(){
        ArrayList<Funcionario> funcionarioList = new ArrayList<>();
        ResultSet resultSet=null;
        try {
            String sql = "SELECT * FROM funcionario";
            stmt = connection.prepareStatement(sql);
            //stmt.setString(1, q);
            resultSet=stmt.executeQuery();
            while(resultSet.next()){
                Funcionario funcionario = new Funcionario();
                funcionario.setId(resultSet.getInt("id_funcionario"));
                funcionario.setNome(resultSet.getString("nome"));
                funcionario.setTelefone(resultSet.getInt("telefone"));
                funcionario.setEmail(resultSet.getString("email"));
                funcionario.setUsuario(resultSet.getString("usuario"));
                funcionario.setSenha(resultSet.getString("senha"));
                
                funcionarioList.add(funcionario);
            }
            return funcionarioList;
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return funcionarioList;
    }
    public void add(Funcionario funcionario){
        String nome = funcionario.getNome();
        int telefone = funcionario.getTelefone();
        String email = funcionario.getEmail();
        String usuario = funcionario.getUsuario();
        String senha = funcionario.getSenha() ;
     
        try{
            String sql = "INSERT INTO funcionario(nome, telefone, email,usuario,senha) VALUES(?,?,?,?,?)";
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, nome);
            stmt.setInt(2, telefone);
            stmt.setString(3, email);
            stmt.setString(4, usuario);
            stmt.setString(5, senha);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha na inserção: "+e);
        }
    }
    
    public void update(Funcionario funcionario){
        int id = funcionario.getId();
        String nome = funcionario.getNome();
        int telefone = funcionario.getTelefone();
        String email = funcionario.getEmail();
        String usuario = funcionario.getUsuario();
        String senha = funcionario.getSenha();
     
        try{
            String sql = "UPDATE funcionario SET nome=?, telefone=?,email=?,usuario=?,senha=? WHERE id_funcionario=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, nome);
            stmt.setInt(2, telefone);
            stmt.setString(3, email);
            stmt.setString(4, usuario);
            stmt.setString(5, senha);
            stmt.setInt(6, id);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha na Actualizar: "+e);
        }
    }
    
    public void delete(int id){
        try{
            String sql = "DELETE FROM funcionario WHERE id_funcionario=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha ao eliminar: "+e);
        }
    }
    public Boolean auth(Funcionario funcionario){
        ResultSet resultSet=null;
        String usuario = funcionario.getUsuario();
        String senha = funcionario.getSenha();
        try {
            String sql = "SELECT * FROM funcionario WHERE usuario=? AND senha=? ";
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, usuario);
            stmt.setString(2, senha);
            resultSet=stmt.executeQuery();
            if(resultSet.next()){
                return true;
            }
            else{
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
