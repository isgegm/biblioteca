package controllers;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Emprestimo;

public class EmprestimoController {
    private Connection connection;
    private Controller controller;
    private PreparedStatement stmt;

    public EmprestimoController() {
        this.connection = new MyConnection().getConnection();
        this.controller = new Controller();
    }
    public ArrayList<Emprestimo> fetch(){
        ArrayList<Emprestimo> emprestimoList = new ArrayList<>();
        ResultSet resultSet=null;
        try {
            String sql = "SELECT id_emprestimo,emprestimo.id_estudante, estudante.nome AS estudante, emprestimo.id_funcionario, funcionario.nome AS funcionario, emprestimo.id_livro, livro.titulo AS livro, data_hora, data_devolucao FROM emprestimo, estudante, funcionario, livro WHERE emprestimo.id_estudante=estudante.id_estudante AND emprestimo.id_funcionario=funcionario.id_funcionario AND emprestimo.id_livro=livro.id_livro ";
            stmt = connection.prepareStatement(sql);
            resultSet=stmt.executeQuery();
            while(resultSet.next()){
                Emprestimo emprestimo = new Emprestimo();
                emprestimo.setId(resultSet.getInt("id_emprestimo"));
                emprestimo.setEstudante(resultSet.getString("estudante"));
                emprestimo.setFuncionario(resultSet.getString("funcionario"));
                emprestimo.setLivro(resultSet.getString("livro"));
                emprestimo.setDataHora(resultSet.getDate("data_hora").toString());
                emprestimo.setDataDevolucao(resultSet.getDate("data_devolucao").toString());
                
                emprestimoList.add(emprestimo);
            }
            return emprestimoList;
        } catch (SQLException ex) {
            Logger.getLogger(EmprestimoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return emprestimoList;
    }
    public void add(Emprestimo emprestimo){
        int idEstudante = emprestimo.getIdEstudante();
        int idFuncionario = emprestimo.getIdFuncionario();
        int idLivro = emprestimo.getIdLivro();
//        Date dataHora = controller.convertDateToDatabase(emprestimo.getDataHora());
     
        try{
            String sql = "INSERT INTO emprestimo(id_estudante, id_funcionario, id_livro) VALUES(?,?,?)";
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, idEstudante);
            stmt.setInt(2, idFuncionario);
            stmt.setInt(3, idLivro);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha na inserção: "+e);
        }
    }
    
    public void update(Emprestimo emprestimo){
        int id = emprestimo.getId();
        int idEstudante = emprestimo.getIdEstudante();
        int idFuncionario = emprestimo.getIdFuncionario();
        int idLivro = emprestimo.getIdLivro();
        Date dataHora = controller.convertDateToDatabase(emprestimo.getDataHora());
     
        try{
            String sql = "UPDATE emprestimo SET id_estudante=?, id_funcionario=?, id_livro=? WHERE id_emprestimo=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, idEstudante);
            stmt.setInt(2, idFuncionario);
            stmt.setInt(3, idLivro);
            stmt.setInt(4, id);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha na Actualizar: "+e);
        }
    }
    
    public void delete(int id){
        try{
            String sql = "DELETE FROM emprestimo WHERE id_emprestimo=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha ao eliminar: "+e);
        }
    }
    
    public void devolver(Emprestimo emprestimo){
        int id = emprestimo.getId();
        Date dataHoraDevolucao = controller.convertDateToDatabase(emprestimo.getDataDevolucao());
     
        try{
            String sql = "UPDATE emprestimo SET data_devolucao=? WHERE id_emprestimo=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setDate(1, dataHoraDevolucao);
            stmt.setInt(2, id);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha na devolucao: "+e);
        }
    }
}
