
package controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Editora;


public class EditoraController {

    private Connection connection;
    private Controller controller;
    private PreparedStatement stmt;

    public EditoraController() {
        this.connection = new MyConnection().getConnection();
        this.controller = new Controller();
    }
    public ArrayList<Editora> fetch(){
        ArrayList<Editora> editoraList = new ArrayList<>();
        ResultSet resultSet=null;
        try {
            String sql = "SELECT * FROM editora";
            stmt = connection.prepareStatement(sql);
            //stmt.setString(1, q);
            resultSet=stmt.executeQuery();
            while(resultSet.next()){
                Editora editora = new Editora();
                editora.setId(resultSet.getInt("id_editora"));
                editora.setNome(resultSet.getString("nome"));
                editora.setEndereco(resultSet.getString("endereco"));
                editora.setIdPais(resultSet.getInt("id_pais"));
                editora.setIdCidade(resultSet.getInt("id_cidade"));
                
                editoraList.add(editora);
            }
            return editoraList;
        } catch (SQLException ex) {
            Logger.getLogger(EditoraController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return editoraList;
    }
    public void add(Editora editora){
        String nome = editora.getNome();
        String endereco = editora.getEndereco();
        int idPais = editora.getIdPais();
        int idCidade = editora.getIdCidade();
     
        try{
            String sql = "INSERT INTO editora(nome, endereco,id_pais,id_cidade) VALUES(?,?,?,?)";
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, nome);
            stmt.setString(2, endereco);
            stmt.setInt(3, idPais);
            stmt.setInt(4, idCidade);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha na inserção: "+e);
        }
    }
    
    public void update(Editora editora){
        int id = editora.getId();
        String nome = editora.getNome();
        String endereco = editora.getEndereco();
        int idPais = editora.getIdPais();
        int idCidade = editora.getIdCidade();
     
        try{
            String sql = "UPDATE editora SET nome=?,endereco=?,id_pais=?,id_cidade=? WHERE id_editora=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, nome);
            stmt.setString(2, endereco);
            stmt.setInt(3, idPais);
            stmt.setInt(4, idCidade);
            stmt.setInt(5, id);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha na Actualizar: "+e);
        }
    }
    
    public void delete(int id){
        try{
            String sql = "DELETE FROM editora WHERE id_editora=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha ao eliminar: "+e);
        }
    }
}

