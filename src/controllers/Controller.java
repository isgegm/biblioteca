package controllers;

import java.text.ParseException;
import java.text.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.KeyValue;

public class Controller {
    public java.sql.Date convertDateToDatabase(String dateToConvert){
        Date date=null;
        java.sql.Date dateConverted = null;
        try {
            date = new SimpleDateFormat("dd-MM-yyyy").parse(dateToConvert);
            dateConverted = new java.sql.Date(date.getTime());
            return dateConverted;
        } catch (ParseException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dateConverted;
    }
    public java.sql.Date convertDateTimeToDatabase(String dateToConvert){
        Date date=null;
        java.sql.Date dateConverted = null;
        try {
            date = new SimpleDateFormat("dd-MM-yyyy").parse(dateToConvert);
            dateConverted = new java.sql.Date(date.getTime());
            return dateConverted;
        } catch (ParseException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dateConverted;
    }
    public String convertDate(String pattern,String dateToConvert){
        String date_converted=null;
        DateFormat format = new SimpleDateFormat(pattern);
        date_converted = format.format(dateToConvert);
        return date_converted;
    }
    public HashMap<String, Integer> setToKeyValue(KeyValue [] keyValue){
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        for(int i=0; i<keyValue.length; i++)
        {
            map.put(keyValue[i].getValue(), keyValue[i].getKey());
        }
        return map;
    }
}
