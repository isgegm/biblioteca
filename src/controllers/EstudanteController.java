package controllers;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Estudante;

public class EstudanteController {
    private Connection connection;
    private Controller controller;
    private PreparedStatement stmt;

    public EstudanteController() {
        this.connection = new MyConnection().getConnection();
        this.controller = new Controller();
    }
    public ArrayList<Estudante> fetch(){
        ArrayList<Estudante> estudanteList = new ArrayList<>();
        ResultSet resultSet=null;
        try {
            String sql = "SELECT * FROM estudante";
            stmt = connection.prepareStatement(sql);
            //stmt.setString(1, q);
            resultSet=stmt.executeQuery();
            while(resultSet.next()){
                Estudante estudante = new Estudante();
                estudante.setId_estudante(resultSet.getInt("id_estudante"));
                estudante.setNome(resultSet.getString("nome"));
                estudante.setTelefone(resultSet.getInt("telefone"));
                estudante.setData_nascimento(resultSet.getDate("data_nascimento").toString());
                estudanteList.add(estudante);
            }
            return estudanteList;
        } catch (SQLException ex) {
            Logger.getLogger(EstudanteController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return estudanteList;
    }
    public void add(Estudante estudante){
        String nome = estudante.getNome();
        int telefone = estudante.getTelefone();
        Date data_nascimento = controller.convertDateToDatabase(estudante.getData_nascimento());
     
        try{
            String sql = "INSERT INTO estudante(nome, telefone, data_nascimento) VALUES(?,?,?)";
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, nome);
            stmt.setInt(2, telefone);
            stmt.setDate(3, data_nascimento);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha na inserção: "+e);
        }
    }
    
    public void update(Estudante estudante){
        int id = estudante.getId_estudante();
        String nome = estudante.getNome();
        int telefone = estudante.getTelefone();
        Date data_nascimento = controller.convertDateToDatabase(estudante.getData_nascimento());
     
        try{
            String sql = "UPDATE estudante SET nome=?, telefone=?, data_nascimento=? WHERE id_estudante=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, nome);
            stmt.setInt(2, telefone);
            stmt.setDate(3, data_nascimento);
            stmt.setInt(4, id);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha na Actualizar: "+e);
        }
    }
    
    public void delete(int id){
        try{
            String sql = "DELETE FROM estudante WHERE id_estudante=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha ao eliminar: "+e);
        }
    }
}
