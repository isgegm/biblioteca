package controllers;
import java.sql.*;
public class MyConnection {
    Connection con;
    public Connection getConnection(){
        try{
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost/biblioteca", "root","");
            return con;
        }catch(SQLException e){
            System.out.println("Connection error: "+ e);
        }
        return con;
    }
    public static void main(String [] args){
        MyConnection conn = new MyConnection();
        conn.getConnection();
    }
}