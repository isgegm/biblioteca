
package controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Livro;


public class LivroController {

    private Connection connection;
    private Controller controller;
    private PreparedStatement stmt;

    public LivroController() {
        this.connection = new MyConnection().getConnection();
        this.controller = new Controller();
    }
    public ArrayList<Livro> fetch(){
        ArrayList<Livro> livroList = new ArrayList<>();
        ResultSet resultSet=null;
        try {
            String sql = "SELECT `id_livro`, `titulo`, `edicao`, `ano`, `codigo`, livro.id_editora, editora.nome AS editora FROM livro, editora WHERE livro.id_editora = editora.id_editora; ";
            stmt = connection.prepareStatement(sql);
            //stmt.setString(1, q);
            resultSet=stmt.executeQuery();
            while(resultSet.next()){
                Livro livro = new Livro();
                livro.setId(resultSet.getInt("id_livro"));
                livro.setTitulo(resultSet.getString("titulo"));
                livro.setEdicao(resultSet.getInt("edicao"));
                livro.setAno(resultSet.getInt("ano"));
                livro.setCodigo(resultSet.getString("codigo"));
                livro.setIdEditora(resultSet.getInt("id_editora"));
                livro.setEditora(resultSet.getString("editora"));
                
                livroList.add(livro);
            }
            return livroList;
        } catch (SQLException ex) {
            Logger.getLogger(LivroController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return livroList;
    }
    public void add(Livro livro){
        String titulo = livro.getTitulo();
        int edicao = livro.getEdicao();
        int ano = livro.getAno();
        String codigo = livro.getCodigo() ;
        int idEditora = livro.getIdEditora() ;
     
        try{
            String sql = "INSERT INTO livro(titulo, edicao,ano,codigo, id_editora) VALUES(?,?,?,?,?)";
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, titulo);
            stmt.setInt(2, edicao);
            stmt.setInt(3, ano);
            stmt.setString(4, codigo);
            stmt.setInt(5, idEditora);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha na inserção: "+e);
        }
    }
    
    public void update(Livro livro){
        int id = livro.getId();
        String titulo = livro.getTitulo();
        int edicao = livro.getEdicao();
        int ano = livro.getAno();
        String codigo = livro.getCodigo();
        int idEditora = livro.getIdEditora();
     
        try{
            String sql = "UPDATE livro SET titulo=?,edicao=?,ano=?,codigo=?,id_editora=? WHERE id_livro=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, titulo);
            stmt.setInt(2, edicao);
            stmt.setInt(3, ano);
            stmt.setString(4, codigo);
            stmt.setInt(5, idEditora);
            stmt.setInt(6, id);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha na Actualizar: "+e);
        }
    }
    
    public void delete(int id){
        try{
            String sql = "DELETE FROM livro WHERE id_livro=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.execute();
            connection.close();
        }
        catch(SQLException e){
            System.out.println("Falha ao eliminar: "+e);
        }
    }
//    public static void main(String[]args){
//        LivroController livroController = new  LivroController();
//        System.out.println(livroController.fetch().get(0).getId());
//    }
}

