package models;
import java.util.Date;
public class Estudante {
    private int id_estudante;
    private String nome;
    private int telefone;
    private String data_nascimento;

    public int getId_estudante() {
        return id_estudante;
    }
    public void setId_estudante(int id_estudante) {
        this.id_estudante = id_estudante;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public int getTelefone() {
        return telefone;
    }
    public void setTelefone(int telefone) {
        this.telefone = telefone;
    }
    public String getData_nascimento() {
        return data_nascimento;
    }
    public void setData_nascimento(String data_nascimento) {
        this.data_nascimento = data_nascimento;
    }
}
